const {app, BrowserWindow, Menu} = require('electron')
const path = require('path')
const url = require('url')
const shell = require('electron').shell
const ipcMain = require('electron').ipcMain


  // Keep a global reference of the window object, if you don't, the window will
  // be closed automatically when the JavaScript object is garbage collected.
  let win
  
  function createMainWindow () {
    // Create the browser window.
    win = new BrowserWindow({width: 1200, height: 900})
    // and load the index.html of the app.
    win.loadFile('source/index.html')
    // Open the DevTools.
    win.webContents.openDevTools()
    // Emitted when the window is closed.
    win.on('closed', () => {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      win = null
    })

    var menu = Menu.buildFromTemplate([
        {
            label: 'Menu',
            submenu: [
                {type: 'separator'},
                {
                  label: 'Exit',
                  click(){
                    app.quit()
                  }
                }
            ]
        },
        {
          label: "Info",
          submenu: [
            {
              label: "Reiniciar",
              click(){
                // shell.openExternal('http://jumo.cat')
                app.relaunch();
                app.exit()
              }
            }
          ]
        }
    ])
    Menu.setApplicationMenu(menu);
    return win
  }

  function createDronWindow () {
    // Create the Dron browser window.
    win = new BrowserWindow({
      frame: true,
      transparent: true,
      width: 500,
      height: 500
    })
    // and load the index.html of the app.
    win.loadFile('source/quad.html')
    setTimeout(() => win.setAlwaysOnTop(true), 1000)//El tiempo de espera de la ventana Emergente para que no desaparezca y pueda mantenerse siempre activa "on top"
    win.on('close', function () { win = null })
    win.openDevTools();
    win.show()
    return win
  }
  
  // This method will be called when Electron has finished
  // initialization and is ready to create browser windows.
  // Some APIs can only be used after this event occurs.
  // app.on('ready', createMainWindow)
  
  app.on('ready', () => {
    window1 = createMainWindow();
    window2 = createDronWindow();
  
    ipcMain.on('nameMsg', (event, arg) => {
    console.log("name inside main process is: ", arg); // this comes form within window 1 -> and into the mainProcess
    event.sender.send('nameReply', { not_right: false }) // sends back/replies to window 1 - "event" is a reference to this chanel.
    window2.webContents.send( 'forWin2', arg ); // sends the stuff from Window1 to Window2.
  });

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })
  
  app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (win === null) {
      createMainWindow()
    }
  })
  
  // In this file you can include the rest of your app's specific main process
  // code. You can also put them in separate files and require them here.
  })
const electron = require('electron')
const remote = electron.remote;
const okBtn = document.getElementById('ok')

okBtn.addEventListener('click', function (event) {
    var window = remote.getCurrentWindow();
    window.close();
})
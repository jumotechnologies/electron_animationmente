const electron = require('electron')
const path = require('path')
const BrowserWindow = electron.remote.BrowserWindow
const ipcRenderer = require('electron').ipcRenderer
const btnShowLog = document.getElementById('safeTimerDisplay')
const btnStart = document.getElementById('playBtn')

// s
// var five = require("johnny-five");
//   var board = new five.Board();

//   board.on("ready", function() {

//     var servos = new five.Servos([9, 10]);

//     // Sweep the servo on pin 9 from 0-180 and repeat.
//     servos[0].sweep();
//   });
// a
anime = require('animejs')
var arrayELementos = "Elementos que han salido:\n";
var ejecutarAnimaciones = true;

function sendData (message) {
  ipcRenderer.send('nameMsg', message);
  ipcRenderer.on('nameReply', (event, arg) => {
    console.log(arg) // why/what is not right..
  });
}

function animacion1 () {
  console.log("Animación 0");
  var controlsProgressEl = document.querySelector('#boxes .progress');

  var boxes = anime.timeline({
    direction: 'alternate',
    loop: false,
    easing: 'linear',
    update: function(anim) {
    controlsProgressEl.value = anim.progress;
    }
  });

  boxes
    .add({
      targets: '#boxes .imagenLengua',
      scale: [
        { value: 4},
      ],
      opacity: [
        {value: 1, duration: 3000},
        ,{value: 0},
      ],
      direction: 'alternate',
      delay: function(el, i, l){ return i * 8000},
      autoplay:false,
      easing: 'easeOutExpo',
      loop:false
      });
      document.querySelector('#boxes .play').onclick = boxes.play;
      document.querySelector('#boxes .pause').onclick = boxes.pause;
      document.querySelector('#boxes .restart').onclick = boxes.restart;

      controlsProgressEl.addEventListener('input', function() {
        boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
      });

      ['input','change'].forEach(function(evt) {
        controlsProgressEl.addEventListener(evt, function() {
          boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
        });
      });
      let message = "lengua"
      arrayELementos = arrayELementos + " "+message+"\n";
      /**Para ejecutar una función del otro render process */
      sendData(message);
}
function animacion2 () {
  console.log("Animación 2");
  var controlsProgressEl = document.querySelector('#boxes .progress');

  var boxes = anime.timeline({
    direction: 'alternate',
    loop: false,
    easing: 'linear',
    update: function(anim) {
      controlsProgressEl.value = anim.progress;
    }
  });
  boxes
    .add({
      targets: '#boxes .imagenPiernas',
      scale: [
        { value: 4},
      ],
      opacity: [
        {value: 1, duration: 3000},
        ,{value: 0},
      ],
        direction: 'alternate',
        delay: function(el, i, l){ return i * 8000},
        autoplay:false,
        easing: 'easeOutExpo',
        loop:false
    })
  document.querySelector('#boxes .play').onclick = boxes.play;
  document.querySelector('#boxes .pause').onclick = boxes.pause;
  document.querySelector('#boxes .restart').onclick = boxes.restart;

  controlsProgressEl.addEventListener('input', function() {
    boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
  });

  ['input','change'].forEach(function(evt) {
    controlsProgressEl.addEventListener(evt, function() {
    boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
   });
  });
  let message = "piernas"
  arrayELementos = arrayELementos + " "+message+"\n";
  /**Para enviar el dato al quad */
  sendData(message);
}
function animacion3 () {
  console.log("Animación 3");
  var controlsProgressEl = document.querySelector('#boxes .progress');

  var boxes = anime.timeline({
    direction: 'alternate',
    loop: false,
    easing: 'linear',
    update: function(anim) {
      controlsProgressEl.value = anim.progress;
    }
   });

  boxes
    .add({
      targets: '#boxes .imagenManoDer',
      scale: [
        { value: 4},
      ],
      opacity: [
        {value: 1, duration: 3000},
        ,{value: 0},
      ],
      direction: 'alternate',
      delay: function(el, i, l){ return i * 8000},
      autoplay:false,
      easing: 'easeOutExpo',
      loop:false
    })
    document.querySelector('#boxes .play').onclick = boxes.play;
    document.querySelector('#boxes .pause').onclick = boxes.pause;
    document.querySelector('#boxes .restart').onclick = boxes.restart;

    controlsProgressEl.addEventListener('input', function() {
      boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
    });

    ['input','change'].forEach(function(evt) {
      controlsProgressEl.addEventListener(evt, function() {
        boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
      });
    });
    let message = "mano_derecha"
    arrayELementos = arrayELementos + " "+message+"\n";
    /**Para ejecutar una función del otro render process */
    sendData(message);
}
function animacion4 () {
  console.log("Animación 4");
  var controlsProgressEl = document.querySelector('#boxes .progress');

  var boxes = anime.timeline({
    direction: 'alternate',
    loop: false,
    easing: 'linear',
    update: function(anim) {
      controlsProgressEl.value = anim.progress;
    }
  });

  boxes
    .add({
      targets: '#boxes .imagenManoIzq',
      scale: [
        { value: 4},
      ],
      opacity: [
        {value: 1, duration: 3000},
        ,{value: 0},
      ],
      direction: 'alternate',
      delay: function(el, i, l){ return i * 8000},
      autoplay:false,
      easing: 'easeOutExpo',
      loop:false
    })
  document.querySelector('#boxes .play').onclick = boxes.play;
  document.querySelector('#boxes .pause').onclick = boxes.pause;
  document.querySelector('#boxes .restart').onclick = boxes.restart;

  controlsProgressEl.addEventListener('input', function() {
    boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
  });

  ['input','change'].forEach(function(evt) {
    controlsProgressEl.addEventListener(evt, function() {
      boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
    });
  });
  let message = "mano_izquierda"
  arrayELementos = arrayELementos + " "+message+"\n";
  /**Para ejecutar una función del otro render process */
  sendData(message);
}
function animacion5 () {
  console.log("Animación 5");
  var controlsProgressEl = document.querySelector('#boxes .progress');

  var boxes = anime.timeline({
    direction: 'alternate',
    loop: false,
    easing: 'linear',
    update: function(anim) {
      controlsProgressEl.value = anim.progress;
    }
  });

  boxes
    .add({
      targets: '#boxes .imagenArdown',
      scale: [
        { value: 4},
      ],
      //Frecuencias
      opacity: [
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 }, { value: 0, duration: 41.666666667 }, { value: 1, duration: 41.666666667 },
        ,{value: 0},
      ],
      direction: 'alternate',
        delay: function(el, i, l){ return i * 8000},
        autoplay:false,
        easing: 'easeOutExpo',
        loop:false
    })
  document.querySelector('#boxes .play').onclick = boxes.play;
  document.querySelector('#boxes .pause').onclick = boxes.pause;
  document.querySelector('#boxes .restart').onclick = boxes.restart;

  controlsProgressEl.addEventListener('input', function() {
    boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
  });

  ['input','change'].forEach(function(evt) {
    controlsProgressEl.addEventListener(evt, function() {
      boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
    });
  });
  let message = "flecha_abajo"
  arrayELementos = arrayELementos + " "+message+"\n";
  /**Para ejecutar una función del otro render process */
  sendData(message);
}
function animacion6 () {
  console.log("Animación 6");
  var controlsProgressEl = document.querySelector('#boxes .progress');

  var boxes = anime.timeline({
    direction: 'alternate',
    loop: false,
    easing: 'linear',
    update: function(anim) {
      controlsProgressEl.value = anim.progress;
    }
  });

  boxes
    .add({
      targets: '#boxes .imagenArright',
      scale: [
        { value: 4},
      ],
      opacity: [
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},{value: 0, duration: 37.037037037037037},{value: 1, duration: 37.037037037037037},
        {value: 0, duration: 37.037037037037037},
        ,{value: 0},
      ],
      direction: 'alternate',
      delay: function(el, i, l){ return i * 8000},
      autoplay:false,
      easing: 'easeOutExpo',
      loop:false
    })
  document.querySelector('#boxes .play').onclick = boxes.play;
  document.querySelector('#boxes .pause').onclick = boxes.pause;
  document.querySelector('#boxes .restart').onclick = boxes.restart;

  controlsProgressEl.addEventListener('input', function() {
    boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
  });

  ['input','change'].forEach(function(evt) {
    controlsProgressEl.addEventListener(evt, function() {
      boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
    });
  });
  let message = "flecha_derecha"
  arrayELementos = arrayELementos + " "+message+"\n";
  /**Para ejecutar una función del otro render process */
  sendData(message);
}
function animacion7 () {
  console.log("Animación 7");
  var controlsProgressEl = document.querySelector('#boxes .progress');

  var boxes = anime.timeline({
    direction: 'alternate',
    loop: false,
    easing: 'linear',
    update: function(anim) {
      controlsProgressEl.value = anim.progress;
    }
  });

  boxes
    .add({
      targets: '#boxes .imagenArup',
      scale: [
        { value: 4},
      ],
      opacity: [
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333},
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 }, { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        { value: 0, duration: 33.333333333333333 }, { value: 1, duration: 33.333333333333333 },
        ,{value: 0},
      ],
      direction: 'alternate',
      delay: function(el, i, l){ return i * 8000},
      autoplay:false,
      easing: 'easeOutExpo',
      loop:false
    })
  document.querySelector('#boxes .play').onclick = boxes.play;
  document.querySelector('#boxes .pause').onclick = boxes.pause;
  document.querySelector('#boxes .restart').onclick = boxes.restart;

  controlsProgressEl.addEventListener('input', function() {
    boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
  });

  ['input','change'].forEach(function(evt) {
    controlsProgressEl.addEventListener(evt, function() {
      boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
    });
  });
  let message = "flecha_arriba"
  arrayELementos = arrayELementos + " "+message+"\n";
  /**Para ejecutar una función del otro render process */
  sendData(message);
}
function animacion8 () {
  console.log("Animación 8");
  var controlsProgressEl = document.querySelector('#boxes .progress');

  var boxes = anime.timeline({
    direction: 'alternate',
    loop: false,
    easing: 'linear',
    update: function(anim) {
      controlsProgressEl.value = anim.progress;
    }
  });

  boxes
    .add({
      targets: '#boxes .imagenLeft',
      scale: [
        { value: 4},
      ],
      opacity: [
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 },
        { value: 0, duration: 30.3030303030303030303030 }, { value: 1, duration: 30.3030303030303030303030 }, { value: 0, duration: 30.3030303030303030303030 },
        ,{value: 0},
      ],
      direction: 'alternate',
      delay: function(el, i, l){ return i * 10000},
      autoplay:false,
      easing: 'easeOutExpo',
      loop:false
    });
    document.querySelector('#boxes .play').onclick = boxes.play;
    document.querySelector('#boxes .pause').onclick = boxes.pause;
    document.querySelector('#boxes .restart').onclick = boxes.restart;

    controlsProgressEl.addEventListener('input', function() {
      boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
    });

    ['input','change'].forEach(function(evt) {
      controlsProgressEl.addEventListener(evt, function() {
        boxes.seek(boxes.duration * (controlsProgressEl.value / 100));
      });
    });
    let message = "flecha_izquierda"
    arrayELementos = arrayELementos + " "+message+"\n";
    /**Para ejecutar una función del otro render process */
    sendData(message);
}

function randomFrom(array) {return array[Math.floor(Math.random() * array.length)];}
var animaciones = function animacion () {
  var func = randomFrom([animacion1, animacion2, animacion3, animacion4,
    animacion5, animacion6, animacion7, animacion8]);
  (func)();
}

btnShowLog.addEventListener('click', function (event) {
  const modalPath = path.join('file://', __dirname, 'add.html')
  let win = new BrowserWindow({
    frame: false,
    transparent: true,
    width: 400,
    height: 150
  })
  setTimeout(() => win.setAlwaysOnTop(true), 1000)//El tiempo de espera de la ventana Emergente para que no desaparezca y pueda mantenerse siempre activa "on top"
  win.on('close', function () { win = null })
  win.loadURL(modalPath)
  win.show()
  ejecutarAnimaciones = false;
  var para = document.createElement("p");
  para.className = "registroImagenes";
  var node = document.createTextNode(arrayELementos);
  para.appendChild(node);
  var element = document.getElementById("boxes");
  element.appendChild(para);

  /**Hacer un .txt con elnombre imageneslog.txt
   * de todas las imagenes que tenemos.*/
  fs = require('fs');
  fs.writeFile('imageneslog.txt', arrayELementos, function (err,data) {
  if (err) return console.log(err);
  console.log('Datos exportados');
  });
})

btnStart.addEventListener('click', function (event) {
  console.log("Start activado");

  var reader = new FileReader();

  let contador = 0;
  ejecutarAnimaciones = true;
  
  var myVar = setInterval(myTimer, 5000);

  // server.js
  var express = require('express');
  var app = express();
  var server = require('http').createServer(app);

  //redirect / to our index.html file
  app.get('/', function(req, res,next) {
  res.sendFile(__dirname + '/source/index.html');
  });

  //start our web server and socket.io server listening
  server.listen(3000, function(){
    console.log('listening on *:3000');
  });

  function myTimer() {
    console.log("llego a la funcion de mover dron?");
    if(ejecutarAnimaciones && contador<80){
      animaciones();
      contador++;
      console.log("contador: "+contador);
    }
  }
})


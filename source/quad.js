animes = require('animejs')
const electron = require('electron')
const remote = electron.remote;
const ipcRenderer = require('electron').ipcRenderer

// //La dependencia de Johnnyfive para controlar el arduino
// var five = require("johnny-five");
// var board = new five.Board();//Instanciamos

const okBtn = document.getElementById('ok')
var $drone = $("#drone-container");
var $delay = 0.1;

dronAnimation();

/**
 * @param datosArduino | 0 = derecha | 1 = izquierda | 2 = Arriba | 3 = Abajo |
 */
var datosArduino = "";

okBtn.addEventListener('click', function (event) {
    // var window = remote.getCurrentWindow();
    // window.close();
    /*********Para pruebas*************/
    pruebaMovimientos();
})

window.setInterval(function(){
    ipcRenderer.on('forWin2', function (event, arg){
        console.log(arg);
        if(arg == "lengua"){
            animacionArriba();
        }
        else if (arg == "piernas"){
            animacionBaja();
        }
        else if (arg == "mano_derecha"){
            animacionDerecha();
        }
        else if (arg == "mano_izquierda"){
            animacionIzquierda();
        }
        else if (arg == "flecha_abajo"){
            animacionBaja();
        }
        else if (arg == "flecha_derecha"){
            animacionDerecha();
        }
        else if (arg == "flecha_arriba"){
            animacionArriba();
        }
        else if (arg == "flecha_izquierda"){
            animacionIzquierda();
        }
      });
}, 3000);

function pruebaMovimientos () {
    var delayInMilliseconds = 3000; //2.5 second
    TweenMax.to($drone, 5, {delay:$delay, x:"300px", ease:Linear.easeInOut, repeat:1, yoyo:true});
    setTimeout(function() {
        //your code to be executed after 2.5 second
        TweenMax.to($drone, 5, {delay:$delay, x:"-300px", ease:Linear.easeInOut, repeat:1, yoyo:true});
        var delayInMilliseconds = 3000;
        setTimeout(function() {
            TweenMax.to($drone, 5, {delay:$delay, y:"-300px", ease:Linear.easeInOut, repeat:1, yoyo:true});
            var delayInMilliseconds = 3000;
            setTimeout(function() {
                TweenMax.to($drone, 5, {delay:$delay, y:"300px", ease:Linear.easeInOut, repeat:1, yoyo:true});
            }, delayInMilliseconds);
        }, delayInMilliseconds);
    }, delayInMilliseconds);
}

function dronAnimation () {
    var $p1 = $("#l-prop1");
    var $p2 = $("#l-prop2");
    var $p3 = $("#r-prop1");
    var $p4 = $("#r-prop2");
    var $drone = $("#drone-container");
    var blade;
    var speed = .01;
    var $delay = 1;

    console.log("Estamos en quad.js");
    //Left Blades
    rotateBladeLeft($p1, speed)
    rotateBladeRight($p2, speed)

    //Right Blades
    rotateBladeLeft($p4, speed)
    rotateBladeRight($p3, speed)

    //Hover
    // TweenMax.to($drone, 5, {delay:$delay, y:"-300px", ease:Linear.easeInOut, repeat:-1, yoyo:true});

    //Shadow
    TweenMax.to(shadow, 5, {delay: $delay, scaleX:".2", x:"40%", opacity:".01", ease:Linear.easeInOut, repeat:-1, yoyo:true});

    //Lights
    TweenMax.to(light1, .5, {opacity:".7", ease:Power2.easeNone, repeat:-1, yoyo:true});

    TweenMax.to(light2, .5, {opacity:".7", ease:Power2.easeNone, repeat:-1, yoyo:true});

    function rotateBladeLeft(blade, speed) {
        console.log("Estamos en Rotate Blade Left");
        var spin = "230px";
        TweenMax.to(blade, speed, {x:spin, scaleX:".1", ease:Power2.easeNone, repeat:-1, yoyo:true});
    }
    function rotateBladeRight(blade, speed) {
    var spin = "-35px";
    TweenMax.to(blade, speed, {x:spin, scaleX:".1", ease:Power2.easeNone, repeat:-1, yoyo:true});
    }
}

function animacionDerecha () {
    TweenMax.to($drone, 5, {delay:$delay, x:"300px", ease:Linear.easeInOut, repeat:1, yoyo:true});
}
function animacionIzquierda () {
    TweenMax.to($drone, 5, {delay:$delay, x:"-300px", ease:Linear.easeInOut, repeat:1, yoyo:true});
}
function animacionArriba () {
    TweenMax.to($drone, 5, {delay:$delay, y:"-300px", ease:Linear.easeInOut, repeat:1, yoyo:true});
}
function animacionBaja () {
    TweenMax.to($drone, 5, {delay:$delay, y:"300px", ease:Linear.easeInOut, repeat:1, yoyo:true});
}

// function control () {
//     board.on("ready", function() {
//         var servos = new five.Servos([9, 10]);
      
//         // Sweep the servo on pin 9 from 0-180 and repeat.
//         servos[0].sweep();
//     });
// }
